﻿using Front.Entities;
using System.Net.Http.Headers;
using System.Net;

using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using System.Security.Claims;
using System.Security.Principal;

namespace Front.Services
{
    public class UserListService
    {
        private readonly HttpClient _httpClient;

        public UserListService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new System.Uri("http://localhost:5000/");
        }

        public async Task<List<UserDTO>> getUsers(string JWT)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);

            HttpResponseMessage response = await _httpClient.GetAsync("api/User/list");
            Console.WriteLine(response.Content.ReadAsStringAsync());
            Console.WriteLine("Code de retour de la requete : " + response.StatusCode);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // You can deserialize the response content here if needed
                var result = await response.Content.ReadFromJsonAsync<List<UserDTO>>();
                return result;
            }
            else if(response.StatusCode == HttpStatusCode.Unauthorized)
            {
                return null;
            }
            else
            {
                return null;
            }
        }
    }
}
