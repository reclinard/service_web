﻿using Front.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net;
using System.Security.Claims;
using System.Reflection;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using System.Net.Http.Headers;

namespace Front.Services
{
    public class TasksService
    {
        private readonly HttpClient _httpClient;
        ProtectedLocalStorage protectedSessionStorage;

        public TasksService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new System.Uri("http://localhost:5000/");

            
        }
        public async Task<List<Task_>> getTasks(string JWT)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);

            HttpResponseMessage response = await _httpClient.GetAsync("api/Tasks");
            Console.WriteLine(response.Content.ReadAsStringAsync());

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // You can deserialize the response content here if needed
                var result = await response.Content.ReadFromJsonAsync <List<Task_>>();
                return result;
            }
            else
            {
                return null;
            }
        }

        public async Task<string> addTask(string JWT, TaskCreate task)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);

            HttpResponseMessage response = await _httpClient.PostAsJsonAsync("api/Tasks", task);
            Console.WriteLine(response.Content.ReadAsStringAsync());

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return "ok";
            }
            else
            {
                return null;
            }
        }

        public async Task<string> delTask(string JWT, Task_ task)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);

            HttpResponseMessage response = await _httpClient.DeleteAsync($"api/Tasks/{task.Id}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return "ok";
            }
            else
            {
                return null;
            }
        }

        public async Task<string> modifyTask(string JWT, Task_ task)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);

            TaskCreate task_create = new TaskCreate { Text = task.Text, IsDone = task.IsDone };

            HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"api/Tasks/{task.Id}", task_create);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return "ok";
            }
            else
            {
                return null;
            }
        }


        //public async Task<Task> AuthenticateUser(string username, string password)
        //{

        //    UserLogin model = new UserLogin { Name = username, Pass = password };

        //    HttpResponseMessage response = await _httpClient.PostAsJsonAsync("api/User/login", model);

        //    // Check if the response status code is 200 (OK)
        //    if (response.StatusCode == HttpStatusCode.OK)
        //    {
        //        // You can deserialize the response content here if needed
        //        var result = await response.Content.ReadFromJsonAsync<UserDTO>();
        //        return result;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
    }
}

