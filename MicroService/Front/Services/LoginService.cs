﻿using Front.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net;
using System.Security.Claims;
using System.Reflection;
using System.Net.Http.Json;

namespace Front.Services
{
    public class LoginService
    {
        private readonly HttpClient _httpClient;

        public LoginService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new System.Uri("http://localhost:5000/");
        }

        public async Task<UserNameAndJWT> AuthenticateUser(string username, string password)
        {

            UserLogin model = new UserLogin { Name = username, Pass = password };

            HttpResponseMessage response = await _httpClient.PostAsJsonAsync("api/User/login", model);

            // Check if the response status code is 200 (OK)
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // You can deserialize the response content here if needed
                var result = await response.Content.ReadFromJsonAsync<UserNameAndJWT>();
                return result;
            }
            else
            {
                return null;
            }
        }
    }
}

