﻿using Front.Entities;
using System.Net;

namespace Front.Services
{
    public class RegisterService
    {
        private readonly HttpClient _httpClient;

        public RegisterService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new System.Uri("http://localhost:5000/");
        }

        public async Task<UserDTOForRegister> RegisterUser(string username, string password, string email)
        {

            UserRegister model = new UserRegister { Name = username, Email = email, Password = password };

            HttpResponseMessage response = await _httpClient.PostAsJsonAsync("api/User/register", model);
            Console.WriteLine(response.StatusCode);
            // Check if the response status code is 200 (OK)
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // You can deserialize the response content here if needed
                var result = await response.Content.ReadFromJsonAsync<UserDTO>();
                return new UserDTOForRegister() { Email = result.Email, Name = result.Name, Id = result.Id, ErrorMsg = ""};
            }
            else if (response.StatusCode == HttpStatusCode.Conflict)
            {
                var msg = await response.Content.ReadAsStringAsync();
                return new UserDTOForRegister() { Email = "", Name = "", Id = -1, ErrorMsg=msg };
            }
            else
            {
                var msg = await response.Content.ReadAsStringAsync();
                if (msg == "Invalid Email")
                {
                    msg = "Email invalide :(";
                }

                if (msg == "Invalid Password")
                {
                    msg = "Mot de passe invalide. Il ne doit pas être vide";
                }

                if (msg == "Invalid Name")
                {
                    msg = "Username invalide. Il peut contenir des lettres, des chiffres et les caractères '-' et '_'.";
                }

                return new UserDTOForRegister() { Email = "", Name = "", Id = -1, ErrorMsg = msg };
            }
        }
    }
}
