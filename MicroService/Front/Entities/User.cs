﻿namespace Front.Entities
{
    public class UserDTO
    {
        public int Id { get; set; }
        public required string Name { get; set; }
        public required string Email { get; set; }
        public string Role { get; set; } = "basic";
    }

    public class UserNameAndJWT
    {
        public required int Id { get; set; }
        public required string Name { get; set; }
        public required string JWT { get; set; }
        public required string Role { get; set; }
    }

    public class UserLogin
    {
        public required string Name { get; set; }
        public required string Pass { get; set; }
    }

    public class UserRegister
    {
        public required string Name { get; set; }
        public required string Email { get; set; }
        public required string Password { get; set; }
    }

    public class UserDTOForRegister : UserDTO
    {
        public string ErrorMsg { get; set; } = "";
    
    }
}
