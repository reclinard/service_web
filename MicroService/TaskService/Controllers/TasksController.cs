﻿using Microsoft.AspNetCore.Mvc;
using TaskService.Entities;
using TaskService.Data;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;


namespace TaskService.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskServiceContext _context;

        public TasksController(TaskServiceContext context)
        {
            _context = context;
        }

        // GET api/Tasks/5
        [HttpGet("{user_id}")]
        public async Task<List<Entities.Task>> Get(int user_id)
        {
            return await _context.TaskWithUserId
                .Where(t => t.UserId == user_id)
                .Select(t => TaskToTaskWithUserId(t))
                .ToListAsync();
        }

        // POST api/Tasks/5
        [HttpPost("{user_id}")]
        public async Task<ActionResult<Entities.TaskWithUserId>> CreateTask(int user_id, TaskCreate task)
        {
            int id_ = 0;

            try
            {
                id_ = await _context.TaskWithUserId
                    .Where(t => t.UserId == user_id)
                    .Select(t => t.Id)
                    .MaxAsync();
            }
            catch (InvalidOperationException)
            {
                
            }

            var t = new Entities.TaskWithUserId
            {
                Id = id_ + 1,
                UserId = user_id,
                IsDone = task.IsDone,
                Text = task.Text
            };

            _context.TaskWithUserId.Add(t);
            await _context.SaveChangesAsync();

            return t;
        }

        // PUT api/Tasks/5/6
        [HttpPut("{user_id}/{id}")]
        public async Task<ActionResult<Entities.Task>> Put(int user_id, int id, TaskCreate taskUpdate)
        {
            var existingTask = await _context.TaskWithUserId
                    .FirstOrDefaultAsync(t => t.UserId == user_id && t.Id == id);

            
            if (existingTask == null)
            {
                return NotFound();
            }

            existingTask.Text = taskUpdate.Text;
            existingTask.IsDone = taskUpdate.IsDone;

            _context.Entry(existingTask).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(existingTask);
        }

        // DELETE api/Tasks/5/6
        [HttpDelete("{user_id}/{id}")]
        public async Task<IActionResult> Delete(int user_id, int id)
        {
            var task = await _context.TaskWithUserId
                .FirstOrDefaultAsync(t => t.UserId == user_id && t.Id == id);

            if (task == null)
            {
                return NotFound();
            }

            _context.TaskWithUserId.Remove(task);
            await _context.SaveChangesAsync();

            return Ok(true);
        }

        private static Entities.Task TaskToTaskWithUserId(TaskWithUserId task)
        {
            return new Entities.Task
            {
                Id = task.Id,
                Text = task.Text,
                IsDone = task.IsDone,
            };
        }
    }
}
