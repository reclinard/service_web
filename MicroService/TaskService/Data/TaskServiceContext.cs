﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TaskService.Entities;

namespace TaskService.Data
{
    public class TaskServiceContext : DbContext
    {
        public TaskServiceContext (DbContextOptions<TaskServiceContext> options)
            : base(options)
        {
        }

        public DbSet<TaskService.Entities.TaskWithUserId> TaskWithUserId { get; set; } = default!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskWithUserId>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.UserId });
            });
        }
    }
}
