﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using UserService.Entities;
using TaskService.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace GatewayService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public TasksController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        // api/Tasks
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> getTasks()
        {
            var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
            if (UserId == null)
            {
                return Unauthorized();
            }

            using (var client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:5002/");

                HttpResponseMessage response = await client.GetAsync($"api/Tasks/{UserId}");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = await response.Content.ReadFromJsonAsync<List<TaskService.Entities.Task>>();
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Login failed");
                }
            }
        }

        // api/Tasks
        [Authorize]
        [HttpPost()]
        public async Task<IActionResult> createTask(TaskCreate task)
        {
            var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
            if (UserId == null)
            {
                return Unauthorized();
            }

            // Create an HttpClient instance using the factory
            using (var client = _httpClientFactory.CreateClient())
            {
                // Set the base address of the API you want to call
                client.BaseAddress = new System.Uri("http://localhost:5002/");

                // Send a POST request to the login endpoint
                HttpResponseMessage response = await client.PostAsJsonAsync($"api/Tasks/{UserId}", task);

                // Check if the response status code is 200 (OK)
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // You can deserialize the response content here if needed
                    return Ok();
                }
                else
                {
                    return BadRequest("Create task failed :(");
                }
            }
        }

        // DELETE api/Tasks/5
        [Authorize]
        [HttpDelete("{id}")]

        public async Task<IActionResult> deleteTask(int id)
        {
            var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
            if (UserId == null)
            {
                return Unauthorized();
            }

            // Create an HttpClient instance using the factory
            using (var client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:5002/");

                HttpResponseMessage response = await client.DeleteAsync($"api/Tasks/{UserId}/{id}");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return Ok();
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest("Delete task failed :(");
                }
            }
        }

        [Authorize]
        [HttpPut("{id}")]

        public async Task<IActionResult> modifyTask(int id, TaskCreate taskUpdate)
        {
            var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
            if (UserId == null)
            {
                return Unauthorized();
            }

            // Create an HttpClient instance using the factory
            using (var client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:5002/");

                HttpResponseMessage response = await client.PutAsJsonAsync($"api/Tasks/{UserId}/{id}", taskUpdate);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return Ok();
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest("Delete task failed :(");
                }
            }
        }
    }
}
