﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using UserService.Entities;

namespace GatewayService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public UserController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        // api/User/register
        [HttpPost("register")]
        public async Task<IActionResult> CreateUser(UserCreateModel userPayload)
        {
            if (!VerifyMail(userPayload.Email))
            {
                return BadRequest("Invalid Email");
            }

            if (!VerifyName(userPayload.Name))
            {
                return BadRequest("Invalid Name");
            }

            if (!VerifyPassword(userPayload.Password))
            {
                return BadRequest("Invalid Password");
            }

            // Create an HttpClient instance using the factory
            using (var client = _httpClientFactory.CreateClient())
            {
                // Set the base address of the API you want to call
                client.BaseAddress = new System.Uri("http://localhost:5001/");

                // Send a POST request to the register endpoint
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Users/register", userPayload);

                // Check if the response status code is 200 (OK)
                if (response.StatusCode == HttpStatusCode.Created)
                {
                    // You can deserialize the response content here if needed
                    var result = await response.Content.ReadFromJsonAsync<UserDTO>();
                    return Ok(result);
                }
                else if (response.StatusCode == HttpStatusCode.Conflict)
                {
                    var msg = await response.Content.ReadAsStringAsync();
                    return Conflict(msg);
                }
                else
                {
                    return BadRequest("Register failed");
                }
            }
        }

        // api/User/login
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLogin model)
        {
            // Create an HttpClient instance using the factory
            using (var client = _httpClientFactory.CreateClient())
            {
                // Set the base address of the API you want to call
                client.BaseAddress = new System.Uri("http://localhost:5001/");

                // Send a POST request to the login endpoint
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Users/login", model);

                // Check if the response status code is 200 (OK)
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // You can deserialize the response content here if needed
                    var result = await response.Content.ReadFromJsonAsync<UserDTO>();

                    if (result != null)
                    {
                        string JWT = GenerateJwtToken(result.Id, result.Role);
                        UserNameAndJWT u = new UserNameAndJWT { Id = result.Id, JWT = JWT, Name = model.Name, Role = result.Role };
                        return Ok(u);
                    }
                    else
                    {
                        return StatusCode(500);
                    }
                }
                else
                {
                    return BadRequest("Login failed");
                }
            }
        }

        // api/User/list
        [Authorize]
        [HttpGet("list")]
        public async Task<IActionResult> UsersList()
        {
            var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
            var UserRole = User.Claims.FirstOrDefault(c => c.Type == "UserRole")?.Value;
            Console.WriteLine(UserId + ", role : " + UserRole);
            if (UserId == null || UserRole != "admin")
            {
                return Unauthorized();
            }

            using (var client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:5001/");

                HttpResponseMessage response = await client.GetAsync("api/Users");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = await response.Content.ReadFromJsonAsync<List<UserService.Entities.UserDTO>>();
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Chargement des utilisateurs echoué : " + response.StatusCode);
                }
            }
        }

        private string GenerateJwtToken(int userId, string userRole)
        {
            var claims = new List<Claim>
            {
                // On ajoute un champ UserId dans notre token avec comme valeur userId en string
                new Claim("UserId", userId.ToString()),
                new Claim("UserRole", userRole)
            };

            // On créer la clé de chiffrement
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("YourSecretKeyLongLongLongLongEnough!!!"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // On paramètre notre token
            var token = new JwtSecurityToken(
                issuer: "TodoProject", // Qui a émit le token
                audience: "localhost:5000", // A qui est destiné ce token
                claims: claims, // Les données que l'on veux encoder dans le token
                expires: DateTime.Now.AddMinutes(3000), // Durée de validité
                signingCredentials: creds); // La clé de chiffrement

            // On renvoie le token signé
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private bool VerifyMail(string mail)
        {
            var email = new EmailAddressAttribute();
            return email.IsValid(mail);
        }

        private bool VerifyName(string name)
        {
            return name.All(c => Char.IsLetterOrDigit(c) || c == '_' || c == '-');
        }

        private bool VerifyPassword(string pass)
        {
            return !pass.IsNullOrEmpty();
        }
    }
}
