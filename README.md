# csharp-microservice

## Fonctionnalités de base
- Front / Gateway / UserService / TaskService
- Création d'un compte
- Login / Logout
- CRUD sur les tâches
- Un compte ne voit que ses informations
- Token JWT
- Caractères spéciaux interdits sur les emails

## Autres fonctionnalités
- Affichage d'un message d'erreur en cas de mot de passe incorrect pendant la
phase de connection
- Affichage d'un message d'erreur si un nom d'utilisateur existe déjà pendant 
la phase de création d'un compte
- Stockage des comptes sur une base de données
- Les comptes ont un rôle et les admins peuvent voir la liste de tous les 
utilisateurs
- Stockage des tâches dans une base de données, d'une manière similaire aux comptes

### Remarques sur les rôles
Les utilisateurs ont les rôles 'basic' ou 'admin'. A la création d'un compte, 
celui-ci obtient automatiquemet le rôle 'basic'. Pour donner le rôle 'admin' à 
un compte, il faut faire un PUT sur celui-ci avec le nouveau rôle en appelant
directement le UserService (donc impossible à faire pour un utilisateur lambda 
qui passe par la Gateway).

Les comptes avec un rôle 'admin' peuvent voir la liste de tous les utilisateurs 
dans une page dédiée.

## Fonctionnalités bonus
- Page d'accueil
- Konami code 
- Miroir
- Clicker

## Utilisateurs déjà présents dans la base de donnée (user : mdp) :
- admin : aaa    -> Possède le rôle administrateur
- test : test    -> Rôle basic. Avec 3 tâches
